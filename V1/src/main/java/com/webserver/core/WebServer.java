package com.webserver.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * WebServer主类
 * WebServer是一个Web容器，模拟Tomcat的基础功能，可以管理部署在这里的所有网络应用(若干webapp)
 * 并且可以与客户端(浏览器)进行TCP链接并基于HTTP协议进行交互。从而使得浏览器可以访问当前容器中的所有网络应用中的资源
 *
 * webapp:网络应用，就是俗称的一个网站，每个网络应用的组成通常都包含:页面，素材(视频，图片等)以及用于处理业务的逻辑代码
 *
 */
public class WebServer {
    private ServerSocket serverSocket;

    public WebServer(){
        try {
            System.out.println("正在启动服务端...");
            serverSocket = new ServerSocket(8088);
            System.out.println("服务端启动完毕!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){
        try {
            System.out.println("等待客户端连接...");
            Socket socket = serverSocket.accept();
            System.out.println("一个客户端连接了!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        WebServer server = new WebServer();
        server.start();
    }
}
